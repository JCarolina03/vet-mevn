const mongoose = require('mongoose');
const { Schema } = mongoose;

const Vaccine = new Schema({
    name: String,
    description: String,
    date: Date

});

module.exports = mongoose.model('Vaccine', Vaccine);