const mongoose = require('mongoose');
const { Schema } = mongoose;

const OwnerSchema = new Schema({
    name: String,
    last_name: String,
    phone: String,
    address: String,
    email: String,
    pets: {
        type: Schema.Types.ObjectId,
        ref: "Pet"
    }
});

module.exports = mongoose.model('Owner', OwnerSchema);