const mongoose = require('mongoose');
const { Schema } = mongoose;

const Invoice = new Schema({
    service_id: Number,
    pet_id: Number,
    user_id: Number,
    date: Date,
    quantity: Number,
    total: Number
});

module.exports = mongoose.model('Invoice', Invoice);