const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CardSchema = new Schema({
    name: { type: String },
    price: { type: Number }
});
const Card = mongoose.model('Card', CardSchema);
module.exports = Card;