const mongoose = require('mongoose');
const { Schema } = mongoose;

const User = new Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    email: String,
    created_at: Date,
    updated_at: Date
});

var User = mongoose.model('User', User);

module.exports = User;