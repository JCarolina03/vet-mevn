const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PetSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    age: Number,
    phone: {
        type: String,
        default: '123-456'
    },
    gender: String,
    owner: {
        type: mongoose.Types.ObjectId,
        ref: "Owner"
    },
}, {
    timestamps: true
});

const Pet = new mongoose.model('pets', PetSchema);

module.exports = Pet;