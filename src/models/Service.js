const mongoose = require('mongoose');
const { Schema } = mongoose;

const Service = new Schema({
    name: String,
    price: Number,
    image: {
            type: String,
            default: 'default.jpg'
        }
});

module.exports = mongoose.model('Service', Service);