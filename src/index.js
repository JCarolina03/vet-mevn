/**servidor basico */
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan')
const mongoose = require('mongoose');

const Cards = require('./api/card');
const Pets = require('./api/pet');
const Owners = require('./api/owner');
const Services = require('./api/services');

const app = express();
mongoose.connect('mongodb://localhost/veterinaria')
    .then(db => console.log('DB is connected'))
    .catch(err => console.error(err));

//settings
app.set('port', process.env.PORT || 45000)

app.use(morgan('dev'));
app.use(express.json()); //entiende toda la info que se envia desde el navegador en formato json -> rest api
app.use("/api/cards", Cards);
app.use("/api/pets", Pets);
app.use("/api/owners", Owners);
app.use("/api/services",Services);
//routes

//Static files
app.use(express.static(__dirname + '/public'));

//server is listening
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});