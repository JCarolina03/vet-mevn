const mongoose = require('mongoose');
const Pet = require('../models/Pet');


const findAllPets = (req, res) => {

    Pet.find((err, pets) => {

        err && res.status(500).send(err.message);

        res.status(200).json(pets);

    });

}

const findById = (req, res) => {

    Pet.findById(req.params.id, (err, pet) => {

        err && res.status(500).send(err.message);

        res.status(200).json(pet);
    })

}

const addPet = (req, res) => {

    let pet = new Pet({
        name: req.body.name,
        age: req.body.age,
        phone: req.body.phone,
        gender: req.body.gender,
        owner: req.body.owner
    })

    pet.save((err, pet) => {

        err && res.status(500).send(err.message);

        res.status(200).json(pet);

    });
}

module.exports = { findAllPets, findById, addPet }