const mongoose = require('mongoose');
const Service = require('../models/Service');


const findAllServices = (req, res) => {

    Service.find((err, services) => {

        err && res.status(500).send(err.message);
        res.status(200).json(services);

    });

}

const findById = (req, res) => {

    Service.findById(req.params.id, (err, service) => {

        err && res.status(500).send(err.message);

        res.status(200).json(service);
    })

}

const addService = (req, res) => {

    let service = new Service({
        name: req.body.name,
        price: req.body.price,
        image: req.body.image
    })

    service.save((err, service) => {

        err && res.status(500).send(err.message);

        res.status(200).json(service);

    });
}

module.exports = { findAllServices, findById, addService }