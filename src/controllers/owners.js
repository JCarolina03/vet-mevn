const mongoose = require('mongoose');
const Owner = require('../models/Owner');


const findAllOwners = (req, res) => {

    Owner.find((err, owners) => {

        err && res.status(500).send(err.message);

        res.status(200).json(owners);

    });

}

const findById = (req, res) => {

    Owner.findById(req.params.id, (err, owner) => {

        err && res.status(500).send(err.message);

        res.status(200).json(owner);
    })

}

const addOwner = (req, res) => {

    let owner = new Owner({
        name: req.body.name,
        last_name: req.body.last_name,
        phone: req.body.phone,
        address: req.body.address,
        email: req.body.email
    })

    owner.save((err, owner) => {

        err && res.status(500).send(err.message);

        res.status(200).json(owner);

    });
}

module.exports = { findAllOwners, findById, addOwner }