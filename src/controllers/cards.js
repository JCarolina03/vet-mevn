const mongoose = require('mongoose');
const Card = require('../models/Card');


const findAllCards = (req, res) => {

    Card.find((err, cards) => {

        err && res.status(500).send(err.message);

        res.status(200).json(cards);

    });

}

const findById = (req, res) => {

    Card.findById(req.params.id, (err, card) => {

        err && res.status(500).send(err.message);

        res.status(200).json(card);
    })

}

const addCard = (req, res) => {

    let card = new Card({
        name: req.body.name,
        price: req.body.price,
    })

    card.save((err, card) => {

        err && res.status(500).send(err.message);

        res.status(200).json(card);

    });
}

module.exports = { findAllCards, findById, addCard }