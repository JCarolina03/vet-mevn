const ServiceController = require('../controllers/services');
const express = require('express');

const router = express.Router();

router.get("/all",ServiceController.findAllServices);
router.get("/:id",ServiceController.findById);
router.post("/add",ServiceController.addService);

module.exports = router;