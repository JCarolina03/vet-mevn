const OwnerController = require('../controllers/owners');
const express = require('express');

const router = express.Router();

router.get("/all", OwnerController.findAllOwners);
router.get("/:id", OwnerController.findById);
router.post("/add", OwnerController.addOwner);

module.exports = router;