const PetController = require('../controllers/pets');
const express = require('express');

const router = express.Router();

router.get("/all", PetController.findAllPets);
router.get("/:id", PetController.findById);
router.post("/add", PetController.addPet);

module.exports = router;